interface DiCacheInterface {
    set(key: string, value: any): void;
    get(key: string): any;
    del(key: string): void;
}

class DiCache implements DiCacheInterface {
    set(key: string, value: any): void {
        CacheService.getScriptCache().put(key, value);
    }

    get(key: string): any {
        return CacheService.getScriptCache().get(key);
    }

    del(key: string): void {
        return CacheService.getScriptCache().remove(key);
    }
}
