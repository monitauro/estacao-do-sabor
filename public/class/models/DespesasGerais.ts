class DespesasGerais extends Model {
    plan: string = "Despesas Gerais";

    buscaMes(month: string, year: string) {
        var sql = "select SUM(C) WHERE YEAR(A) = " + year + " AND MONTH(A)+1 = " + month;
        return this.query(sql);
    }
}
