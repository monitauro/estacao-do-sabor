class Comanda extends Model {
    plan: string = "Comanda";

    arrange(date: string): Comanda {
        var gdata: any[] = [];

        this.data.forEach(function(item) {
            gdata.push([date, item]);
        });

        this.data = gdata;

        return this;
    }

    buscaMes(month: string, year: string) {
        var sql = "select SUM(B) WHERE YEAR(A) = " + year + " AND MONTH(A)+1 = " + month;
        return this.query(sql);
    }
}
