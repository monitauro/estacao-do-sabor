class FuncionariosPagamentos extends Model {
    plan: string = "Funcionários Pagamentos";

    buscaMes(month: string, year: string) {
        var sql = "select SUM(D) WHERE YEAR(A) = " + year + " AND MONTH(A)+1 = " + month;
        return this.query(sql);
    }
}
