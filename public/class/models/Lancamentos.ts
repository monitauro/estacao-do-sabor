class Lancamentos extends Model {
    plan: string = "Lançamentos";

    arrange(date: string): Lancamentos {
        // var taxa = '=IF($C2="";"";$E2*(VLOOKUP($C2;\'Stone - Bandeiras\'!$A$2:$D$18;IF($D2="Débito";2;IF($D2="Crédito";3;4));FALSE)/100))';
        // var valor_menos_taxa = "=$E2-$F2";
        var taxa = '=IF($C%row%="";"";$E%row%*(VLOOKUP($C%row%;\'Stone - Bandeiras\'!$A$2:$D$18;IF($D%row%="Débito";2;IF($D%row%="Crédito";3;4));FALSE)/100))';
        var valor_menos_taxa = "=$E%row%-$F%row%";
        var movimentacao = "";
        var gdata: any[] = [];

        this.data.forEach(function(item, index) {
            var ntaxa = taxa.replace(/%row%/g, (index+2).toString());
            var nvalor_menos_taxa = valor_menos_taxa.replace(/%row%/g, (index+2).toString());

            movimentacao = item.valor > 0 ? "ENTRADA" : item.valor < 0 ? "SAIDA" : "";

            gdata.push([date, item.metodo, item.bandeira, item.tipo, item.valor, ntaxa, nvalor_menos_taxa, movimentacao]);
        });

        // console.log(this.data);
        this.data = gdata.reverse();

        return this;
    }

    buscaMes(month: string, year: string, metodo: string) {
        var sql = "select SUM(G) WHERE B='" + metodo + "' AND YEAR(A) = " + year + " AND MONTH(A)+1 = " + month;
        return this.query(sql);
    }
}
