interface DiInterface {
    set(key: string, func: Function): Di;
}

class Di implements DiInterface {
    set(key: string, func: Function): Di {
        this[key] = func();

        return this;
    }
}
