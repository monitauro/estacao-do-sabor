interface ModelInterface {
    plan: string;
    data: any[];

    setData(data: any[]): Model;
    insert(): Model;
    delete(row: number): Model;
    deleteMult(rows: number[]): Model;
    find(row: number, column: number, data: any): number[];
}

class Model implements ModelInterface {
    plan: string;
    data: any[];

    setData(data: any[]): Model {
        this.data = data;

        return this;
    }

    insert(): Model {
        var regs: number = this.data.length;
        var spreadsheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(this.plan);

        spreadsheet.getRange("2:2").activate();
        spreadsheet.insertRowsBefore(spreadsheet.getActiveRange().getRow(), regs);

        regs++;
        this.data.forEach(function(item) {
            item.forEach(function(sitem, sindice) {
                var cell = String.fromCharCode(64 + 1 + sindice).concat(regs.toString());
                spreadsheet.getRange(cell).activate();
                spreadsheet.getCurrentCell().setValue(sitem);
            });
            regs--;
        });

        spreadsheet.getRange("A1").activate();
        return this;
    }

    delete(row: number): Model {
        var spreadsheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(this.plan);
        spreadsheet.deleteRow(row);

        spreadsheet.getRange("A1").activate();
        return this;
    }

    deleteMult(rows: number[]): Model {
        if (rows.length) {
            rows = rows.reverse();
            for (var row = 0; row < rows.length; row++) {
                this.delete(rows[row]);
            }
        }

        return this;
    }

    find(row: number, column: number, data: any): number[] {
        var spreadsheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(this.plan);
        var localsearch = spreadsheet.getRange(row, column, spreadsheet.getLastRow()).getDisplayValues();

        var rows: number[] = [];

        for (var row = 0; row < localsearch.length - 1; row++) {
            if (localsearch[row][0].toString() == data.toString()) {
                rows.push(row + 2);
            }
        }

        return rows;
    }

    query(request: string) {
        var cspreadsheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(this.plan);
        var clrow = cspreadsheet.getLastRow();

        var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
        var sheet = spreadsheet.insertSheet().hideSheet();
        var sql = "=QUERY('" + this.plan + "'!1:"+clrow+";\"" + request + '";1)';
        console.log(sql);
        var r = sheet.getRange(1, 1).setFormula(sql);
        var reply = sheet.getDataRange().getDisplayValues();

        // di["nav"].setSheet(this.plan).activeSheet();

        spreadsheet.deleteSheet(sheet);

        reply.shift();

        return reply;
    }
}
