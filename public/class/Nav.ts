interface NavInterface {
    sheet: string;
    default_sheet: string;
    current_sheet: string;
    activeSpreadsheet: any;

    setSheet(sheet: string): Nav;
    setCurrentSheet(sheet: string): Nav;
    init(): void;
    activeSheet(): void;
    menu(): void;
}

class Nav implements NavInterface {
    sheet: string;
    default_sheet: string;
    current_sheet: string;
    activeSpreadsheet: any;

    constructor(default_sheet: string) {
        this.activeSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();
        this.default_sheet = default_sheet;

        // busca planilha previamente ativada
        var current_sheet: string = di["cache"].get("current_sheet");

        if (current_sheet == undefined || current_sheet == null) {
            // inicializa na planilha home
            current_sheet = this.default_sheet;
            di["cache"].set("current_sheet", current_sheet);
        }

        this.setCurrentSheet(current_sheet);
    }

    setSheet(sheet: string): Nav {
        this.sheet = sheet;

        return this;
    }

    setCurrentSheet(sheet: string): Nav {
        this.current_sheet = sheet;

        return this;
    }

    init(): void {
        // define planilha a ser aberta e abre a planilha
        var current_sheet = this.activeSpreadsheet.getSheetByName(this.current_sheet).showSheet();

        var sheets: any = this.activeSpreadsheet.getSheets();
        for (var i = 0; i < sheets.length; i++) {
            if (sheets[i].getName() != this.current_sheet) {
                sheets[i].hideSheet();
            }
        }

        // this.menu();
    }

    showForm(tpl: string, h: number, w: number, list: any[] = []): void {
        var ui = SpreadsheetApp.getUi();
        var Form = HtmlService.createTemplateFromFile("tpl/" + tpl);
        if (list.length) {
            Form.list = list;
        }
        var showForm = Form.evaluate();

        showForm.setHeight(h).setWidth(w);
        ui.showModalDialog(showForm, " ");
    }

    activeSheet(): void {
        if (this.sheet != this.current_sheet) {
            // define planilha a ser aberta e abre a planilha
            var plan = this.activeSpreadsheet.getSheetByName(this.sheet);
            plan.showSheet();
            SpreadsheetApp.setActiveSheet(plan);
            // move planilha ativa para primeira possicao
            this.activeSpreadsheet.moveActiveSheet(1);
            // define planilha a ser ocultada e oculta planilha aberta
            this.activeSpreadsheet.getSheetByName(this.current_sheet).hideSheet();

            // define planilha corrente
            this.current_sheet = this.sheet;
            di["cache"].set("current_sheet", this.current_sheet);
        }
    }

    menu(): void {
        var ui = SpreadsheetApp.getUi();
        var Form = HtmlService.createTemplateFromFile("tpl/menu");
        var showForm = Form.evaluate();

        showForm.setTitle("MENU");

        ui.showSidebar(showForm);
    }

    processando(): void {
        this.showForm("processando", 50, 200);
    }

    alert(msg: string) {
        SpreadsheetApp.getUi().alert(msg); //.addToUi()
    }
}
