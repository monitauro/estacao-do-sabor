var di: Di = new Di();

di.set("cache", function() {
    return new DiCache();
})
    .set("nav", function() {
        return new Nav("Home");
    })
    .set("utils", function() {
        return new Utils();
    })
    .set("comanda", function() {
        return new Comanda();
    })
    .set("lancamentos", function() {
        return new Lancamentos();
    })
    .set("movimentacao-por-mes", function() {
        return new MovimentacaoPorMes();
    })
    .set("funcionarios-pagamentos", function() {
        return new FuncionariosPagamentos();
    })
    .set("funcionarios", function() {
        return new Funcionarios();
    })
    .set("custos-fixos", function() {
        return new CustosFixos();
    })
    .set("despesas-operaconais", function() {
        return new DespesasOperacionais();
    })
    .set("despesas-gerais", function() {
        return new DespesasGerais();
    });

/**
 * onOpen function
 * é chamada quando se abre a planilha
 */

function onOpen() {
    di["nav"].init();
}

// navegação entre planilhas
function _hyperlink(sheet: string) {
    switch (sheet) {
        case "fechamento-de-caixa":
            di["nav"].showForm("fechamento-de-caixa", 780, 860);
            di["nav"].setSheet("Lançamentos").activeSheet();

            break;
        case "funcionarios-pagamentos":
            // pega nome de funcionarios
            var rows = di["funcionarios"].list();

            di["nav"].showForm("funcionarios-pagamentos", 390, 860, rows);
            di["nav"].setSheet("Funcionários Pagamentos").activeSheet();

            break;
        case "lancar-despesas":
            di["nav"].showForm("lancar-despesas", 280, 530);
            di["nav"].setSheet("Despesas Gerais").activeSheet();
            break;
        default:
            di["nav"].setSheet(sheet).activeSheet();

            break;
    }
}
function isEmpty(value) {
    return value == null || value.length === 0;
}
// abre menu
function menu() {
    di["nav"].menu();
}

function run_actions(data) {
    di["nav"].processando();

    data.forEach(function(item) {
        switch (item[0]) {
            case "comanda_insert":
                di["nav"].setSheet(di["comanda"].plan).activeSheet();
                // busca ocorrencias da mesma data
                var rows = di["comanda"].find(2, 1, item[1]);
                // delete ocorrencias da mesma data
                di["comanda"].deleteMult(rows);
                // insere valor de comanda da mesma data
                di["comanda"]
                    .setData(item[2])
                    .arrange(item[1])
                    .insert();

                di["nav"].setSheet(di["comanda"].plan).activeSheet();
                di["nav"].alert("Fechamento de caixa realizado!");

                break;
            case "lacamentos_insert":
                di["nav"].setSheet(di["lancamentos"].plan).activeSheet();
                // busca ocorrencias da mesma data
                var rows = di["lancamentos"].find(2, 1, item[1]);
                // delete ocorrencias da mesma data
                di["lancamentos"].deleteMult(rows);
                // insere valor de comanda da mesma data
                di["lancamentos"]
                    .setData(item[2])
                    .arrange(item[1])
                    .insert();
                di["nav"].setSheet(di["lancamentos"].plan).activeSheet();

                break;
            case "funcionarios_insert":
                var gdata: any[] = [];

                item[2].forEach(function(item) {
                    gdata.push([item.data, item.funcionario, item.tipo, item.valor]);
                });

                di["funcionarios-pagamentos"].setData(gdata).insert();
                di["nav"].alert("Pagamento lançado!");

                break;
            case "lancar_despesas":
                var gdata: any[] = [];

                gdata.push([item[2].data, item[2].descricao, item[2].valor]);

                di["despesas-gerais"].setData(gdata).insert();
                di["nav"].alert("Despesa lançada!");

                break;
            case "fechar-mes":
                di["nav"].setSheet(di["movimentacao-por-mes"].plan).activeSheet();

                var mesCorrente = di["utils"].getMes();
                var arrDate = mesCorrente.split("/");
                var gdata: any[] = [];

                // busca ocorrencias da mesma data
                var rows = di["movimentacao-por-mes"].find(2, 1, mesCorrente);
                // delete ocorrencias da mesma data
                di["movimentacao-por-mes"].deleteMult(rows);

                // pega pagamentos
                di["nav"].setSheet(di["funcionarios-pagamentos"].plan).activeSheet();
                rows = di["funcionarios-pagamentos"].buscaMes(arrDate[0], arrDate[1]);
                if (Object.keys(rows).length) {
                    gdata.push([mesCorrente, "Funcionários", rows[0][0]]);
                }

                // pega custos fixos
                di["nav"].setSheet(di["custos-fixos"].plan).activeSheet();
                rows = di["custos-fixos"].buscaMes();
                for (var i in rows) {
                    gdata.push([mesCorrente, rows[i][0], rows[i][1]]);
                }

                // pega Despesas Operacionais
                di["nav"].setSheet(di["despesas-operaconais"].plan).activeSheet();
                rows = di["despesas-operaconais"].buscaMes();
                for (var i in rows) {
                    if (rows[i][0] && rows[i][1]) {
                        gdata.push([mesCorrente, rows[i][0], rows[i][1]]);
                    }
                }

                // pega Despesas Gerais
                di["nav"].setSheet(di["despesas-gerais"].plan).activeSheet();
                rows = di["despesas-gerais"].buscaMes(arrDate[0], arrDate[1]);
                if (Object.keys(rows).length) {
                    gdata.push([mesCorrente, "Despesas Gerais", rows[0][0]]);
                }

                // pega Comandas
                di["nav"].setSheet(di["comanda"].plan).activeSheet();
                rows = di["comanda"].buscaMes(arrDate[0], arrDate[1]);
                if (Object.keys(rows).length) {
                    gdata.push([mesCorrente, "Comanda", rows[0][0]]);
                }
                di["nav"].setSheet(di["comanda"].plan).activeSheet();

                // pega CARTOES
                di["nav"].setSheet(di["lancamentos"].plan).activeSheet();
                rows = di["lancamentos"].buscaMes(arrDate[0], arrDate[1], "CARTÃO");
                if (Object.keys(rows).length) {
                    gdata.push([mesCorrente, "CARTÃO", rows[0][0]]);
                }
                di["nav"].setSheet(di["lancamentos"].plan).activeSheet();

                // pega DINHEIRO
                di["nav"].setSheet(di["lancamentos"].plan).activeSheet();
                rows = di["lancamentos"].buscaMes(arrDate[0], arrDate[1], "DINHEIRO");
                if (Object.keys(rows).length) {
                    gdata.push([mesCorrente, "DINHEIRO", rows[0][0]]);
                }

                // pega PENDURA/Entrada
                di["nav"].setSheet(di["lancamentos"].plan).activeSheet();
                rows = di["lancamentos"].buscaMes(arrDate[0], arrDate[1], "PENDURA/Entrada");
                if (Object.keys(rows).length) {
                    gdata.push([mesCorrente, "PENDURA/Entrada", rows[0][0]]);
                }

                // pega PENDURA/Saída
                di["nav"].setSheet(di["lancamentos"].plan).activeSheet();
                rows = di["lancamentos"].buscaMes(arrDate[0], arrDate[1], "PENDURA/Saída");
                if (Object.keys(rows).length) {
                    gdata.push([mesCorrente, "PENDURA/Saída", rows[0][0]]);
                }

                // insere movimentacoes do mes
                di["nav"].setSheet(di["movimentacao-por-mes"].plan).activeSheet();
                di["movimentacao-por-mes"].setData(gdata).insert();
                di["nav"].alert("Fechamento do mês realizado");

                break;

            default:
                break;
        }
    });
}
